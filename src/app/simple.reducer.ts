import { Action } from '@ngrx/store';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';

/**
 * Handles the actions for ngrx/store
 * @param state 
 * @param action 
 */
export function simpleReducer(state: any = 'Happy New Year', action: Action) {
  switch (action.type) {

    case 'MESSAGE_ONE':
      return state = 'Happy New Year Will';

    case 'MESSAGE_TWO':
      return state = 'Happy New Year Marat';

    // Update the state to the current device motion data
    case 'MOTION':
      let motion = new DeviceMotion();
      return state = motion.getCurrentAcceleration().then(
        (acceleration: DeviceMotionAccelerationData) => {
          return acceleration;
        },
        (error: any) => {
          return error;
        }
      );

    default:
      return state;
  }
}