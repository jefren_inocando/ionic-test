import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
import { BackgroundMode } from '@ionic-native/background-mode';
import { DatePipe } from '@angular/common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

interface AppState {
  message: any;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // holds device motion data to be used on view
  data: any;

  // stores the state
  message: Observable<any>;

  constructor(public navCtrl: NavController,
    private deviceMotion: DeviceMotion,
    private backgroundMode: BackgroundMode,
    private store: Store<AppState>,
    private datePipe: DatePipe
  ) {

    // enable this app to run in the background
    this.backgroundMode.enable();

    // get device motion data every 1 second
    this.deviceMotion.watchAcceleration({ frequency: 1000 }).subscribe(
      (acceleration: DeviceMotionAccelerationData) => {

        // used to display DeviceMotionAccelerationData on the view
        this.data = acceleration;

        // update notification bar with DeviceMotionAccelerationData to show it is running as a background process
        this.backgroundMode.configure({
          title: "Device motion watcher is running in background",
          hidden: false,
          text: "X: " + acceleration.x + " Y: " + acceleration.y + " Z: " + acceleration.z + " Time: " + this.datePipe.transform(acceleration.timestamp, "mediumTime")
        });
      }
    );

    // triggers MOTION action to update state every 5 seconds
    setInterval(() => { this.store.dispatch({ type: 'MOTION' }); }, 5000);

    // get latest state from ngrx/store
    this.message = store.select('message');

  }

}
