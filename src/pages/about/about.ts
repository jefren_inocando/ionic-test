import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

interface AppState {
  message: any;
}

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  // stores the state
  message: Observable<any>;

  constructor(public navCtrl: NavController, private store: Store<AppState>) {

    // gets any new state in the store
    this.message = store.select('message');
  }

  // triggers MESSAGE_ONE action which returns a new messageF
  getMessageOne() {
    this.store.dispatch({ type: 'MESSAGE_ONE' });
  }

  // triggers MESSAGE_TWO action which returns a new message
  getMessageTwo() {
    this.store.dispatch({ type: 'MESSAGE_TWO' });
  }

  // triggers MOTION action which returns the device motion data
  getMotionData() {
    this.store.dispatch({ type: 'MOTION' });
  }

}
