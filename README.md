# README #

Ionic test

Used the following modules:

- https://ionicframework.com/docs/native/device-motion/
- https://github.com/ngrx/platform/blob/master/docs/store/README.md
- https://ionicframework.com/docs/native/background-mode/

### Setup ###

Go to the root folder run the ff commands:

- `npm install`
- Please make sure android studio emulator is running or your test mobile device is connected.
- `ionic cordova run android`
